/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.nichiwa_system.taskBoard;

import static org.hamcrest.Matchers.*;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author okumura
 */
public class TaskBoardTest {
    
    private TaskBoard sut;
    private Stage first, second, third;
    
    @Before
    public void setUp() {
        sut = new TaskBoard(
                first = new Stage("1st"),
                second = new Stage("2nd"),
                third = new Stage("3rd")
        );
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void 追加したタスクは最初のエリアに入る() {
        assertThat(first.getTasks(),
                not(hasItem(hasProperty("description", is("新しいタスク")))));
        
        sut.newTask("新しいタスク");
        
        assertThat(first.getTasks(),
                hasItem(hasProperty("description", is("新しいタスク"))));
    }

    @Test
    public void タスクを移動すると移動先に入る() {
        sut.newTask("foo");
        sut.newTask("bar");
        sut.newTask("buzz").moveTo(second);
        Task foo = first.getTasks().get(0);
        
        assertThat(second.getTasks(), not(hasItem(foo)));
        
        foo.moveTo(second);
        
        assertThat(first.getTasks(), not(hasItem(foo)));
        assertThat(second.getTasks(), hasItem(foo));
    }
    
    @Test
    public void ステージ番号からStageを取得できる() {
        assertEquals(first, sut.getStage(first.getId()));
    }
    
    @Test
    public void タスク番号からTaskを取得できる() {
        sut.newTask("foo");
        sut.newTask("bar");
        Task buzz = sut.newTask("buzz");
        
        assertEquals(buzz, sut.getTask(buzz.getId()));
    }
}
