/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.nichiwa_system.taskBoard;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;
import org.junit.Test;

/**
 *
 * @author okumura
 */
public class StageTest {
    @Test
    public void 一引数コンストラクタは空のステージを作る() {
        Stage stage = new Stage("Test");
        
        assertThat(stage.getName(), is("Test"));
        assertThat(stage.getTasks(), is(empty()));
    }
}
