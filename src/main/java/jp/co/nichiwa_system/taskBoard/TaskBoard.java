/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package jp.co.nichiwa_system.taskBoard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 *
 * @author okumura
 */
public class TaskBoard implements Serializable {

    private final Stage firstArea;
    private final List<Task> tasks;
    private final List<Stage> stages;

    public TaskBoard(Stage first, Stage... others) {
        firstArea = first;
        
        List<Stage> list = new ArrayList<>();
        
        list.add(first);
        list.addAll(Arrays.asList(others));
        
        this.stages = Collections.unmodifiableList(list);
        
        tasks = new ArrayList<>(this.stages.stream().flatMap(area ->
                    area.getTasks().stream()).collect(Collectors.toList())
        );
    }

    public List<Stage> getStages() {
        return stages;
    }
    
    public List<Task> getTasks() {
        return tasks;
    }

    Task newTask(final String description) {
        Task task = new Task(description, firstArea);
        tasks.add(task);
        return task;
    }

    Stage getStage(final UUID id) {
        return stages.stream().filter(stage->stage.getId().equals(id)).findFirst().get();
    }

    Task getTask(UUID id) {
        return tasks.stream().filter(task->task.getId().equals(id)).findFirst().get();
    }
}
