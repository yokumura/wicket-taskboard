package jp.co.nichiwa_system.taskBoard;

import java.util.UUID;
import org.apache.wicket.Component;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class BoardPage extends WebPage {

    private static final long serialVersionUID = 1L;

    private TaskBoard board;
    private Component boardView;

    public BoardPage(final PageParameters parameters) {
        super(parameters);

        board = new TaskBoardRepository().getTaskBoard(parameters.get("boardId").toString());
        
        add(new RepeatingView("stages") {

            @Override
            protected void onInitialize() {
                super.onInitialize();
                add(board.getStages().stream()
                    .map(stage->new Label(newChildId(), stage.getName()))
                    .toArray(i->new Component[i])
                );
            }
        });

        add(boardView = new WebMarkupContainer("body").add(new ListView<Stage>("areas", board.getStages()) {
            @Override
            protected void populateItem(ListItem<Stage> li) {
                li.add(
                    new HiddenField("stageId", new PropertyModel(li.getModelObject(), "id")),
                    new RepeatingView("task") {{
                        add(li.getModelObject().taskStream().map(task->
                            new WebMarkupContainer(newChildId()) {{
                                add(
                                    new HiddenField("taskId", new PropertyModel(task, "id")),
                                    new Label("description", task.getDescription())
                                );
                            }}
                        ).toArray(i->new Component[i]));
                    }}
                );
            }
        }).setOutputMarkupId(true));

        add(new Form<Void>("newTask") {
            
            public String description;

            @Override
            protected void onInitialize() {
                super.onInitialize();
                
                add(new TextArea<String>("description", new PropertyModel(this, "description")));
                add(new AjaxFallbackButton("add", this) {
                    @Override
                    protected void onSubmit(AjaxRequestTarget target, Form<?> form) {
                        board.newTask(description);
                        description = "";

                        target.add(boardView, form);
                        target.appendJavaScript("setupDragNDrop();");
                    }
                });
            }
        }.setOutputMarkupId(true));

        add(new Form<Void>("moveTask") {
                public String taskId, stageId;

                @Override
                protected void onInitialize() {
                    super.onInitialize();
                    
                    add(new HiddenField<>("taskId", new PropertyModel<String>(this, "taskId")));
                    add(new HiddenField<>("stageId", new PropertyModel<String>(this, "stageId")));
                }

                @Override
                protected void onSubmit() {
                    board.getTask(UUID.fromString(taskId)).moveTo(
                            board.getStage(UUID.fromString(stageId)));
                }
            }
        );
    }
}
