/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.nichiwa_system.taskBoard;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author okumura
 */
public class Task implements Serializable {
    
    private final UUID id;
    private final String description;
    
    private Stage stage;

    UUID getId() {
        return id;
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    
    public Task(String description, Stage stage) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.stage = stage;
        stage.addTask(this);
    }

    void moveTo(Stage newStage) {
        this.stage.removeTask(this);
        newStage.addTask(this);
        
        this.stage = newStage;
        
    }
    
    boolean isIn(Stage stage) {
        return this.stage.equals(stage);
    }
    
    @Override
    public String toString() {
        return "task:" + description;
    }
}
