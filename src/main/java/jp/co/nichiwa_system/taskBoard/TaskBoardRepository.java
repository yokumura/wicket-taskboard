/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.nichiwa_system.taskBoard;

import java.util.stream.Stream;

/**
 *
 * @author okumura
 */
public class TaskBoardRepository {

    TaskBoard getTaskBoard(String id) {
        Stage todo, doing, done;
        final TaskBoard taskBoard = new TaskBoard(
                todo = new Stage("Todo"),
                doing = new Stage("Doing"),
                done = new Stage("Done")
        );
        taskBoard.newTask("最初のタスク").moveTo(done);
        taskBoard.newTask("次のタスク").moveTo(doing);
        Stream.of("三度目のタスク", "最後のタスク").forEach(d->taskBoard.newTask(d));
        
        return taskBoard;
        
    }
    
}
