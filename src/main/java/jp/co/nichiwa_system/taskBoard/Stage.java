/*
 * Copyright 2014 okumura.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package jp.co.nichiwa_system.taskBoard;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

/**
 *
 * @author okumura
 */
public class Stage implements Serializable {
    
    private final UUID id;
    private final String name;
    private final List<Task> tasks;
    
   Stage(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.tasks = new ArrayList<>();
    }
    
    public List<Task> getTasks() {
        return tasks;
    }
    
    public Stream<Task> taskStream() {
        return tasks.stream();
    } 

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    
    void addTask(Task task) {
        tasks.add(task);
    }
    
    void removeTask(Task task) {
        tasks.remove(task);
    }

    UUID getId() {
        return id;
    }
 }
